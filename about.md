---
layout: page
title: About
permalink: /about/
---

Economist by training, data scientist by trade, living in Durham, NC. Like everyone else, I am on [LinkedIn](https://www.linkedin.com/in/ghuiber) too. 

### Eno River

The Eno flows through Durham and it's pretty. Though you should hike in the [park](http://www.ncparks.gov/Visit/parks/enri/main.php) and give money to the [conservancy](http://www.enoriver.org), this blog has nothing to do with either.

I worked freelance between 2007 and 2010. I called my shop Eno River Analytics and I claimed this domain because it was available. 

I could have picked a better name. I speak with an accent. When I say "Eno River Analytics" people tend to hear "in a river analytics" and are rightly confused. 